
package com.fed.hellorest.ejb;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;

@Stateful
@SessionScoped
    public class HitCounterSessionBean implements Serializable
{
    private final AtomicInteger hitCounter = new AtomicInteger(0);

    public int getCount() {
        return hitCounter.getAndIncrement();
    }
}
