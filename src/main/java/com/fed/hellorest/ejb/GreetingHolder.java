/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fed.hellorest.ejb;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;

@Stateful
@SessionScoped
public class GreetingHolder implements Serializable {
    
    private String greeting = "HELLO";
    
    public String getGreeting() {
        return greeting;
    }
    
    public void setGreeting(String newGreeting) {
        greeting = newGreeting;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
