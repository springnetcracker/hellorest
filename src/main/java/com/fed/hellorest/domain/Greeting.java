/*
 * This software is the confidential information and copyrighted work of
 * NetCracker Technology Corp. ("NetCracker") and/or its suppliers and
 * is only distributed under the terms of a separate license agreement
 * with NetCracker.
 * Use of the software is governed by the terms of the license agreement.
 * Any use of this software not in accordance with the license agreement
 * is expressly prohibited by law, and may result in severe civil
 * and criminal penalties. 
 * 
 * Copyright (c) 1995-2015 NetCracker Technology Corp.
 * 
 * All Rights Reserved.
 */
package com.fed.hellorest.domain;

/**
 *
 * @author anfe0815
 */
public class Greeting
{
    public static final String DEF_GREETING = "hi";
    
    public static final String DEF_SIGNATURE = "from greeter";
    
    private String value;
    
    private String signature;

    public Greeting()
    {
        this(DEF_GREETING, DEF_SIGNATURE);
    }
    
    public Greeting(String value, String signature) {
        this.value = value;
        this.signature = signature;
    }
    
    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getSignature()
    {
        return signature;
    }

    public void setSignature(String signature)
    {
        this.signature = signature;
    }
    
}
