
package com.fed.hellorest;

import com.fed.hellorest.domain.Greeting;
import com.fed.hellorest.ejb.GreetingHolder;
import com.fed.hellorest.ejb.HitCounterSessionBean;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("greeting")
public class GreetingResource {
    
    @Inject
    HitCounterSessionBean hitCounterBean;
    
    @Inject 
    GreetingHolder greetingHolder;

    @Context
    private UriInfo context;

    public GreetingResource() {
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public String getHello(@PathParam("id") int id) {
        return "<h3>" + greetingHolder.getGreeting() + "#"+ id +"<h3>"
                + "<h4>hitCount=" + hitCounterBean.getCount() + "<h4>";
    }
    
    @POST
    public void setGreeting(@FormParam("greeting") String newGreeting) {
        greetingHolder.setGreeting(newGreeting);
    }
    
    @GET
    @Path("json/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Greeting getGreeting(@PathParam("id") int id) {
        Greeting g = new Greeting("HELLO" + id, "from Greeting Resource");
        return g;
    }
    
    @POST
    @Path("json")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Greeting setGreeting(Greeting newGreeting) {
        greetingHolder.setGreeting(newGreeting.getValue());
        return new Greeting(newGreeting.getValue() + "#",
                newGreeting.getSignature() + "#");
    }
    
}
