package com.fed.hellorest.com.istratenko;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by denis on 26.03.16.
 */
@Path("templates")
public class Template {

    /**
     * Create new Template
     */
    @POST
    public Response createTemplate(){
        return Response.ok().build(); //if template successfully create
    }

    /**
     * Get template by template_id
     * @param template_id
     */
    @GET
    @Path("{template_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Template getTemplate(@PathParam("template_id") int template_id){
        Template template = new Template(); //get template by id from DB
        return template;

    }

    /**
     * Update template by template_id
     * @param template_id
     */
    @PUT
    @Path("{template_id}")
    public Response updateTemplate(@PathParam("template_id") int template_id){
        return Response.ok().build(); //if template successfully update

    }

    /**
     * delete template by template_id
     * @param template_id
     */
    @DELETE
    @Path("{template_id}")
    public Response deleteTemplate(@PathParam("template_id") int template_id){
        //if report_id does not exist in DB, then response must be with code 500 (Resposnse.serverError().build)
        return Response.noContent().build();
    }

    /**
     * Get all templates for current connection by connection_id
     * @param connection_id
     */
    @GET
    @Path("?connection_id={connection_id}")
    public Iterator<Template> getAllTemplateForCurrConn(@PathParam("connection_id") int connection_id){
        List<Template> templateList = new ArrayList<Template>();
        return templateList.iterator();
    }

    /**
     * Get template params for current template by template_id
     * @param template_id
     */
    @GET
    @Path("template_params?template_id={template_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getTemplParamsForCurrTempl(@PathParam("template_id") int template_id){

    }

}
