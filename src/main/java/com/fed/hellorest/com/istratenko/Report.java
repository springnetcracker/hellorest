package com.fed.hellorest.com.istratenko;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created by denis on 26.03.16.
 */
@Path("reports")
public class Report {


    /**
     * Create new Report
     */
    @POST
    public Response createReport(){
        return Response.ok().build(); //if report successfully create
    }

    /**
     * Get report by report_id
     * @param report_id
     */
    @GET
    @Path("{report_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Report getReport(@PathParam("report_id") int report_id) {
        Report report = new Report(); //get report by id from DB
        return report;
    }

    /**
     * update report by report_id
     * @param report_id
     */
    @PUT
    @Path("{report_id}")
    public Response updateReport(@PathParam("report_id") int report_id){
        return Response.ok().build(); //if report successfully update

    }

    /**
     * delete report by report_id
     * @param report_id
     */
    @DELETE
    @Path("{report_id}")
    public Response deleteReport(@PathParam("report_id") int report_id){
        //if report_id does not exist in DB, then response must be with code 500 (Resposnse.serverError().build)
        return Response.noContent().build(); //response code 204
    }

}
